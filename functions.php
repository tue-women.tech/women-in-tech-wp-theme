<?php
function my_theme_enqueue_styles() {
    $parent_style = 'wp_bootstrap_starter_style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
	wp_enqueue_style( 'fullpage', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.2/fullpage.min.css' );
	wp_enqueue_style( 'iconic', 'https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css' );
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

function my_theme_enqueue_scripts() {
	wp_enqueue_script( 'fullpage-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.2/fullpage.min.js', array(), false, true );	
}

add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_scripts' );

function my_theme_jquery() {
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array(), false, true );
 }

if (!is_admin()) add_action("wp_enqueue_scripts", "my_theme_jquery", 11);

// Register Widgets

// Footer Widgets
if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Footer links',
        'id'   => 'main-footer-left-widget',
        'description'   => 'individuelles Widget',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer rechts',
        'id'   => 'main-footer-right-widget',
        'description'   => 'individuelles Widget',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));

    register_sidebar(array(
        'name' => 'Footer Bottom rechts',
        'id'   => 'main-footer-bottom-right-widget',
        'description'   => 'individuelles Widget',
        'before_widget' => '<div id="%1$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}

/**
 * Change price format from range to "Ab" in Woocommerce
 *
 * @param float $price
 * @param obj $product
 * @return str
 */

function iconic_variable_price_format( $price, $product ) {
    $prefix = sprintf('%s ', __('ab', 'iconic'));
    $min_price_regular = $product->get_variation_regular_price( 'min', true );
    $min_price_sale    = $product->get_variation_sale_price( 'min', true );
    $max_price = $product->get_variation_price( 'max', true );
    $min_price = $product->get_variation_price( 'min', true );

    $price = ( $min_price_sale == $min_price_regular ) ?
        wc_price( $min_price_regular ) :
        '<del>' . wc_price( $min_price_regular ) . '</del>' . '<ins>' . wc_price( $min_price_sale ) . '</ins>';

    return ( $min_price == $max_price ) ?
        $price :
        sprintf('%s%s', $prefix, $price);
}

add_filter( 'woocommerce_variable_sale_price_html', 'iconic_variable_price_format', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'iconic_variable_price_format', 10, 2 );

?>