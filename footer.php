<?php
/**
 * Footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
    <?php get_template_part( 'footer-widget' ); ?>
<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
	<div class="container pt-3 pb-3" id="copyright">
	<div class="row">
		<div class="col-6">
			<div class="site-info">
                &copy; <?php echo date('Y'); ?>, <?php echo '<a href="'.home_url().'">'.get_bloginfo('name').'</a>'; ?>
			</div>
		</div>
		<!-- close .site-info -->
		<div class="col-6">
			<?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('main-footer-bottom-right-widget') ) ?>
		</div>
	</div>
	</div>
</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->
<!-- /Footer Ende -->
<?php wp_footer(); ?>

<!-- Skripte -->
<script>
	new fullpage('#fullpage', {
        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        autoScrolling: true,
        navigation: true,
		navigationTooltips: ['Willkommen!', 'Wer sind wir?', 'Was wollen wir?', 'Wie machen wir das?', 'Das erwartet dich', 'Beitreten'],
		scrollHorizontally: false,
		responsiveWidth: 768
    });
</script>
<!-- /Skripte -->

</body>
</html>